A game of Pong commanded by an Arduino board. 

# Commands

This is commanded via a light detector. By moving your hand from the light detector you can lower the light received, which lowers the paddle. Raising your hand raises the paddle. 

# Gitlab integration

Each the ball hit the paddle, it sends a commit to this repo https://gitlab.com/JoBarthel/git-pong via Gitlab's API. 
When the game starts, it loads the game state from the latest commit at this repo. 

# Dependencies

HTTP requests thanks to https://github.com/runemadsen/HTTP-Requests-for-Processing (available through Processing library manager)

PostRequest.java from https://github.com/acdean/HTTP-Requests-for-Processing/tree/master/src/http/requests to allow for POST requests from JSON.