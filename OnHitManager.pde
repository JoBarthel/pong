class OnHitManager implements OnHitCallback {

  Score score;
  HttpRequestHandler requestHandler;
  
  OnHitManager(Score score, HttpRequestHandler requestHandler)
  {
    this.score = score;
    this.requestHandler = requestHandler;
  }
  
  void execute(OnHitInfo onHitInfo)
  {
    score.increment();
    String uniqueName = hashDate();
    Action[] actions = { new Action("create", uniqueName, formatGameState(onHitInfo)) };
    ThreadedCommit threadedCommit = new ThreadedCommit(new Commit("master", formatGameState(onHitInfo), actions  ), requestHandler);
    threadedCommit.start();
  }
    
  String hashDate()
  {
    return year() + "/" + month() + "/" + day() + "-" + hour() + ":" + minute() + ":" + second() ;
  }
  
  String formatGameState(OnHitInfo onHitInfo) 
  {
     StringBuilder builder = new StringBuilder();
     builder.append(score.getScore());
     builder.append(" ");
     builder.append(onHitInfo.ballPosition.x);
     builder.append(" ");
     builder.append(onHitInfo.ballPosition.y);
     builder.append(" ");
     builder.append(onHitInfo.ballVelocity.x);
     builder.append(" ");
     builder.append(onHitInfo.ballVelocity.y);
     builder.append(" ");
     builder.append(onHitInfo.paddlePosition.x);
     builder.append(" ");
     builder.append(onHitInfo.paddlePosition.y);
     return builder.toString();
  }
   
}

class ThreadedCommit extends Thread {

   Commit commit;
   HttpRequestHandler requestHandler;
   ThreadedCommit(Commit commit, HttpRequestHandler requestHandler)
   {
     this.commit = commit;
     this.requestHandler = requestHandler;
   }
   
   void run() {
     requestHandler.makeCommit(commit);
   }
}
