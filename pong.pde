import processing.serial.*;
import cc.arduino.*;

final int LIGHT_SENSOR = 0;
final String PROJECT_ID = "15468620";

HttpRequestHandler requestHandler = new HttpRequestHandler("http://gitlab.com/api/v4", "EgAT8FasBPh9WJakziRk", PROJECT_ID);
Score score = new Score();
OnHitManager onHitManager = new OnHitManager(score, requestHandler);

Boundaries window = new Boundaries(800, 600);
Paddle paddle = new Paddle( new Vector2(40, 100), new Vector2(20, 100));
Ball ball = new Ball(new Vector2(8.0, 8.0) , new Vector2(100, 100), 50, window, paddle, onHitManager);

Arduino arduino = new Arduino(this, Arduino.list()[0], 57600);
Calibration calibration = new Calibration(50, 100);
ArduinoPaddle arduinoPaddle = new ArduinoPaddle(paddle, arduino, calibration, window.sizeY);


void setup()
{
  size(800, 600);
  arduino.pinMode(LIGHT_SENSOR, Arduino.INPUT);
  GameState gameState = requestHandler.loadGame();
  ball.position = gameState.ballPosition;
  ball.velocity = gameState.ballVelocity;
  paddle.position = gameState.paddlePosition;
  score.set(gameState.score);
} 

void draw() {

  clear();
  ball.move();
  ball.draw();
  arduinoPaddle.move();
  arduinoPaddle.draw();
  score.display(window, new Vector2(200, 50 ));
  
}
