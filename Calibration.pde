class Calibration 
{
  float min;
  float max;
  Calibration(float min, float max)
  {
    this.min = min;
    this.max = max;
  }
  
  float calibrate( float originalValue, float toMin, float toMax )
  {
    updateCalibration(originalValue);
    return map(originalValue, min, max, toMin, toMax);
    
  }
  
  private void updateCalibration(float value)
  {
    if(value < min) min = value;
    if(value > max) max = value;
  }
}
