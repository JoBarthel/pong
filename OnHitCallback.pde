interface OnHitCallback{
  void execute(OnHitInfo onHitInfo);
}

class OnHitInfo {
  
  OnHitInfo(Vector2 ballPosition, Vector2 ballVelocity, Vector2 paddlePosition)
  {
    this.ballPosition = ballPosition;
    this.ballVelocity = ballVelocity;
    this.paddlePosition = paddlePosition;
  }
  
  Vector2 ballPosition;
  Vector2 ballVelocity;
  Vector2 paddlePosition;
}
