import http.requests.*;

class HttpRequestHandler 
{
    String baseUrl;
    String privateToken;
    String projectId;
    
    HttpRequestHandler(String url, String privateToken, String projectId)
    {
      this.baseUrl = url;
      this.privateToken = privateToken;
      this.projectId = projectId;
    
    }
    
    GameState loadGame() {
      
      GetRequest get = new GetRequest(commitsUrl());
      get.addHeader("PRIVATE-TOKEN", privateToken);
      get.send();
      JSONArray json = parseJSONArray(get.getContent());
      if(json == null)  {
        println("json could not be parsed");
        return null;
      }
       else
       {
         String message = json.getJSONObject(0).getString("message");
         String[] elements = split(message, ' ');
        return new GameState( int(elements[0]), 
               new Vector2(float(elements[1]), float(elements[2])),
                                               new Vector2(float(elements[3]), float(elements[4])), 
                                               new Vector2(float(elements[5]), float(elements[6]))  );
       }
      
    }
    
    void makeCommit(Commit commit)
    {
      PostRequest post = new PostRequest("https://gitlab.com/api/v4/projects/"+ projectId +"/repository/commits");
      post.addHeader("PRIVATE-TOKEN", privateToken);
      post.addJson(commit.toJson());
      post.send();
      println("Response Content: " + post.getContent());
      println("Response Content-Length Header: " + post.getHeader("Content-Length"));
    }
    
    private String commitsUrl()
    {
      return baseUrl+"/projects/"+projectId+"/repository/commits";  
    }
    
}
