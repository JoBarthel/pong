class GameState{
  int score;
  Vector2 ballPosition;
  Vector2 ballVelocity;
  Vector2 paddlePosition;
  
  GameState(int score, Vector2 ballPosition, Vector2 ballVelocity, Vector2 paddlePosition)
  {
   this.score = score;
   this.ballPosition = ballPosition;
   this.ballVelocity = ballVelocity;
   this.paddlePosition =  paddlePosition;
  }
}
