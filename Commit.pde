class Commit {
  String branch;
  String message;
  Action[] actions;
  
  Commit(String branch, String message, Action[] actions) 
  { 
    this.branch = branch;
    this.message = message;
    this.actions = actions;
  }
  
  String toJson() 
  {
    StringBuilder builder = new StringBuilder();
    builder.append("{ \"branch\": \"" + branch + "\",");
    builder.append(" \"commit_message\": \"" + message + "\",");
    builder.append(" \"actions\": [");
    boolean isFirstObject = true;
    for(Action action: actions)
    {
      if(!isFirstObject)
        builder.append(",");
      isFirstObject = false;
      builder.append( action.toJson());
    }
    builder.append("] }");
    return builder.toString();
  }
  
}

class Action {
  String action;
  String filePath;
  String content;
  
  Action(String action, String filePath, String content)
  {
    this.action = action;
    this.filePath = filePath;
    this.content = content;
  }
  
  String toJson()
  {
    return "{  \"action\": \""   + action   + "\", " + 
            "\"file_path\": \""   + filePath + "\", " + 
            "\"content\":  \""   + content  + "\" " +
            "}";
  }
}
