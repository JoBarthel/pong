class Paddle {
  Vector2 position;
  Vector2 size;
  Paddle(Vector2 position, Vector2 size) {
    this.position = position;
    this.size = size;
  }
  
  void draw() {
     rect( position.x, position.y, size.x, size.y);   
  }
  
  void move(float positionY) {
    position.y = positionY;
  }
  
  Vector2 extents() {
    return new Vector2(position.x + size.x / 2, position.y + size.y / 2);
  }
  
  Vector2 bottoms() {
    return new Vector2(position.x - size.x / 2, position.y - size.y / 2);
  }
  boolean contains(Vector2 point) 
  {
    return point.x > position.x && point.x < extents().x && point.y > position.y && point.y < extents().y; 
  }
  
  Vector2 intersection(Ball ball)
  {
      float xComponent = 0;
      float yComponent = 0;
     if(ball.position.x > position.x)
       xComponent = ball.position.x - ball.radius - extents().x;
     else
       xComponent = bottoms().x - (ball.position.x + ball.radius);
      if(ball.position.y > position.y)
        yComponent = ball.position.y - ball.radius - extents().y;
      else
        yComponent = bottoms().y - (ball.position.y + ball.radius);

     return new Vector2(xComponent, yComponent);
     
  }
}
