class ArduinoPaddle {
  Paddle paddle;
  Arduino arduino;
  float sizeY;
  
  ArduinoPaddle(Paddle paddle, Arduino arduino, Calibration calibration, float sizeY)
  {
    this.paddle = paddle;
    this.arduino = arduino;
    this.sizeY = sizeY;
  }
  
  void move()
  {
    float value = arduino.analogRead(LIGHT_SENSOR);
    paddle.move( calibration.calibrate(value, 0.0f, sizeY));
  }
  
  void draw() 
  {
    paddle.draw();
  }
}
