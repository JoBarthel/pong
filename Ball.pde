class Ball
{
  Vector2 position;
  Vector2 velocity;
  float radius;
  HttpRequestHandler requestHandler;
  OnHitCallback onHit;  
  Boundaries boundaries;
  Ball(Vector2 velocity, Vector2 position, float radius, Boundaries boundaries, Paddle paddle, OnHitCallback onHit)
  {
    this.velocity = velocity;
    this.radius = radius;
    this.position = position;
    this.onHit = onHit; 
    this.boundaries = boundaries;
  }
  
  void draw()
  {
    circle(position.x, position.y , radius);
  }
  
  void move()
  {
    position.x += velocity.x;
    position.y += velocity.y;
    checkCollisionsWithBorders();
    checkCollisionsWithPaddle();
  }
  
  void checkCollisionsWithBorders(){
    if(position.x > boundaries.sizeX || position.x < 0)
      velocity.x = -velocity.x;
    if(position.y > boundaries.sizeY ||position.y < 0)
      velocity.y = -velocity.y; 
  }
  
  void checkCollisionsWithPaddle() 
  {
    Vector2 intersection = paddle.intersection(ball);
    if(intersection.x < 0 && intersection.y < 0)
    {
      velocity.x = - velocity.x;
      onHit.execute(new OnHitInfo(position, velocity, paddle.position)); 
    }
  }
  //<>//
}
