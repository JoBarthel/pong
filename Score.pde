class Score{
  int currentScore = 0;
  void increment() {
    currentScore += 1;
  }
  
  int getScore() 
  {
    return currentScore;
  }
  
  void set(int newScore)
  {
    currentScore = newScore;
  }
  
  void display(Boundaries boundaries, Vector2 padding)
  {
    textSize(32);
    text("Score: " + currentScore, boundaries.sizeX - padding.x, padding.y);
  }
}
